FROM python:3.10.13-slim AS final

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

ARG DEBUG
ENV FLASK_DEBUG=${DEBUG:-false}
ENV FLASK_RUN_HOST=0.0.0.0
COPY . .

CMD flask run
