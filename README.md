# TBO

## Wstęp

### Zespół

- Bartłomiej Moroz,
- Jakub Motyka,

### Odnośniki

- Repozytorium projektu: [GitLab](https://gitlab.com/MortonPL/tbo-23z) (GitLab CI),
- Rejestr obrazów: [Docker Hub](https://hub.docker.com/repository/docker/mortonpl/tbo-23z/general),
- Repozytorium wykorzystanej aplikacji: [GitHub](https://github.com/MohammadSatel/Flask_Book_Library)
  (aplikacja z laboratoriów, Python).

## Zadanie 1 - zaprojektowanie procesu CI/CD

### Konfiguracja repozytorium

- Repozytorium jest widoczne publicznie,
- Każdy użytkownik może forkować repozytorium,
- Tylko członkowie zespołu są uprawnieni do tworzenia gałęzi,
- Tylko członkowie zespołu są uprawnieni do tworzenia *Merge Request*,
- Tylko członkowie zespołu z rolą **Maintainer** (lub wyższą) są uprawnieni do akceptowania i
  scalania *Merge Request*,
- Tylko członkowie zespołu z rolą **Owner** są uprawnieni do bezpośredniego dokonywania zmian
  na gałęzi `main`.

### Proces CI/CD

Dla każdej gałęzi, przy każdym nowym commicie uruchamiany jest proces CI/CD,
którego kroki przedstawiono poniżej. Jeżeli którykolwiek krok zakończy się niepowodzeniem, proces
jest przerywany. Po ukończeniu procesu testowania tworzony jest obraz konteneru Docker dla tego MR,
który jest następnie publikowany w publicznym rejestrze obrazów.

1. Testy bezpieczeństwa sekretów i konfiguracji: [`Gitleaks`](https://gitleaks.io),
1. Testy konfiguracji pliku `Dockerfile`: [`checkov`](https://www.checkov.io),
1. Testy jednostkowe: [`pytest`](https://pypi.org/project/pytest/),
1. Testy bezpieczeństwa SCA: [`safety`](https://pypi.org/project/safety/) wykorzystujące bazę
   [Safety DB](https://github.com/pyupio/safety-db),
1. Testy bezpieczeństwa SAST: [`bandit`](https://pypi.org/project/bandit/),
1. Testy bezpieczeństwa API DAST: [`OWASP ZAP - API Scan`](https://www.zaproxy.org/docs/docker/api-scan/).
   *(Test przeprowadzany jest na tymczasowo zbudowanej wersji aplikacji, która jest usuwana po
   skończeniu skanu)*
1. Testy bezpieczeństwa pełne DAST: [`OWASP ZAP - Full Scan`](https://www.zaproxy.org/docs/docker/full-scan/).
   *(Test przeprowadzany jest na tymczasowo zbudowanej wersji aplikacji, która jest usuwana po
   skończeniu skanu)*
1. Zbudowanie obrazu kontenera i opublikowanie go.
   Obraz jest otagowany jako `latest` dla gałęzi `main` i `beta` dla innych.

Dla testów jednostkowych, konfiguracji `Dockerfile` oraz SAST, wyniki były dostępne w formacie JUnit
(XML), więc podłączyliśmy je do wbudowanego w GitLab panelu podsumowania wyników.
Wyniki pozostałych należy czytać bezpośrednio z konsoli.

Ponieważ nieprzechodzące testy blokują możliwość zbudowania i opublikowania obrazu na DockerHub,
aby zaprezentować tą możliwość tymczasowo wyłączyliśmy testy.
[Przykładowy pipeline budujący tag `latest`](https://gitlab.com/MortonPL/tbo-23z/-/pipelines/1154131120)
[Przykładowy pipeline budujący tag `beta`](https://gitlab.com/MortonPL/tbo-23z/-/pipelines/1154127531)

### Wyniki

[Wyniki pipeline'a dla oryginalnego kodu](https://gitlab.com/MortonPL/tbo-23z/-/jobs/6029354944).

#### Dockerfile

Skan pliku `Dockerfile` wskazuje, że brakuje w nim dwóch rzeczy:

- nie jest zdefiniowany użytkownik aplikacji, użyty jest użytkownik root,
- brakuje polecenia do monitorowania życia aplikacji, tzw. *healthcheck*.

Podczas gdy pierwszy punkt jest istotny i należy go poprawić przed wystawieniem aplikacji na świat,
punkt drugi jest tylko dobrą praktyką, którą więcej osób powinno zacząć stosować w swoich
aplikacjach kontenerowych.

Pozostałe, przechodzące testy weryfikują, że nie próbowaliśmy wprowadzić żadnych dodatkowych luk
w bezpieczeństwie, np. poprzez wyłączenie weryfikacji certyfikatów.

#### Skany DAST

Dla skanu API zdiagnozowanych zostało wiele problemów, m.in.:

- ciasteczka nie są ograniczone atrybutem `SameSite`,
- pobierane są skrypty JS obcego pochodzenia (poza naszą kontrolą),
- brak tokenów zapobiegających CSRF,
- serwer udostępnia swoją wersję w atrybucie `Server`,
- brakuje nagłówków zapobiegających atakom *clickjacking*.

Skan pełny jest w większości identyczny, najciekawsze wydaje się nowe wystąpienie
`Cookie Slack Detector [90027]`. Odnosi się ono do niebezpiecznego sposobu obsługi ciasteczek,
który może zdradzać gdzie ciasteczka nie są wymagane, co jest potencjalną luką w zabezpieczeniach.

#### Pozostałe

Gitleaks nie wykrył wycieku żadnych sekretów, chociaż klucz symetryczny jest zapisany w kodzie.

Testy jednostkowe przeklejone z trzecich laboratoriów naturalnie wykryły wiele problemów z modelem
książki, np. podatność na XSS i SQL-Inj.

Statyczna analiza aplikacji wykryła tylko jeden problem - wystąpienie klucza symetrycznego w kodzie
aplikacji.

Statyczna analiza komponentów wykryła przestarzałą wersję paczki `werkzeug:2.3.7`,
której wolne parsowanie zapytań multipart może być podstawą do ataku DoS.

## Zadanie 2 - weryfikacja działania procesu CI/CD

Pod koniec pracy nad projektem wykorzystaliśmy limit darmowego czasu pracy runnerów, ale zdążyliśmy
zebrać wyniki dla oryginalnego kodu i dla wprowadzonych podatności.

### Wstrzyknięcie SQL

[Wyniki pipeline'a dla podatności](https://gitlab.com/MortonPL/tbo-23z/-/pipelines/1154086728).

Endpoint aplikacji do pobrania szczegółów książek został zmodyfikowany, aby zamiast bezpiecznego API
bazy danych korzystał z surowego zapytania SQL.
Nazwa książki podana przez użytkownika jest bezpośrednio wklejana do ciągu znaków zapytania.
Warto zauważyć, że nazwa ta jest podawana przez parametr `Query` zapytania, a nie przez jego `Body`.

Ręczne wykorzystanie podatności pozwala na ucieczkę z cytatów, czyli modyfikację filtra `select`,
ale wykorzystane API bazy danych nie pozwala na wywołanie kolejnego polecenia w jednym zapytaniu,
np. `'; drop table book; select '` nie zostaje wykonane.

Wektor ataku został wykryty przez statyczną analizę aplikacji:
![SQL_Injection](./images/sql_injection/sast.png)

Zarówno dynamiczny skan API jak i całej aplikacji nie wykryły bezpośrednio nowej podatności mimo,
że test `PASS: SQL Injection - SQLite [40024]` powinien to weryfikować.
Zmiany w aplikacji zostały zdiagnozowane głównie jako błędny typ zwrotny "Internal Server Error 500".
Powodem na to może być skorzystanie z `Query`, zamiast `Body` zapytania HTTP, ale również
ograniczenie do jednego polecenia w zapytaniu SQL (próba wstrzyknięcia kolejnego kończy się błędem).

### Path traversal

[Wyniki pipeline'a dla podatności](https://gitlab.com/MortonPL/tbo-23z/-/pipelines/1154091098/).

Do aplikacji dodano nową funkcjonalność: wgrywanie dowolnego obrazka jako okładki książki. Kod
realizujący zapisywanie i wysyłanie obrazków jest jednak bardzo niebezpieczny: zapisuje obrazki
jako pliki na dysku z nazwą odpowiadającą nazwie książki (i zamianą spacji na `_`). Dodany także
został nowy endpoint serwujący obrazki na bazie nazwy, aby można je było wyświetlić na liście
książek.

Atakujący tworzy "złośliwą" książkę, która nadpisuje inny plik.
![Path traversal 1](./images/path_traversal/before_and_after.png)

Atakujący pobiera dowolny plik na maszynie serwera wykorzystująć API.
![Path traversal 2](./images/path_traversal/download.png)

Ponieważ zarówno nazwa książki, jak i ścieżka nie są w żaden sposób weryfikowane, atakujący może
wstawić ścieżkę względną jako nazwę książki i umieścić plik w dowolnym miejsc lub nadpisać
istniejący, np. `../data.sqlite`. Endpoint do otrzymywania obrazków tak samo jest w stanie wysłać
dowolny plik, o jaki poprosi wysyłający.

Zaskakujące jest, że *nic* nie wykryło tak rażącej podatności: zarówno statyczna analiza (brak
komentarza), jak i dynamiczny skan API i całej aplikacji (`PASS: Path Traversal [6]`).
